<?php

namespace Drupal\ebrsww\Plugin\EntityBrowser\Widget;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Reference a remote file using remote stream wrapper.
 *
 * @EntityBrowserWidget(
 *   id = "remote_media",
 *   label = @Translation("Remote media"),
 *   description = @Translation("Reference remote media using Remote stream wrapper.")
 * )
 */
class MediaEntityBrowser extends FileEntityBrowser {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
        'media_type' => NULL,
      ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $aditional_widget_parameters) {
    /** @var \Drupal\media\MediaTypeInterface $bundle */
    if (!$this->configuration['media_type'] || !($bundle = $this->entityTypeManager->getStorage('media_type')->load($this->configuration['media_type']))) {
      return ['#markup' => $this->t('The media bundle is not configured correctly.')];
    }

    if ($bundle->getSource()->getPluginId() != 'image') {
      return ['#markup' => $this->t('The configured bundle is not using image plugin.')];
    }

    $form = parent::getForm($original_form, $form_state, $aditional_widget_parameters);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntities(array $form, FormStateInterface $form_state) {
    $files = parent::prepareEntities($form, $form_state);

    /** @var \Drupal\media\MediaTypeInterface $bundle */
    $bundle = $this->entityTypeManager
      ->getStorage('media_type')
      ->load($this->configuration['media_type']);

    $entities = [];
    foreach ($files as $file) {
      /** @var \Drupal\media\MediaInterface $media */
      $media = $this->entityTypeManager->getStorage('media')->create([
        'bundle' => $bundle->id(),
        $bundle->get('source_configuration')['source_field'] => $file,
      ]);
      $entities[] = $media;
    }

    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $bundle_options = $this->bundleOptions();

    if (empty($bundle_options)) {
      $url = Url::fromRoute('media.bundle_add')->toString();
      $form['media_type'] = [
        '#markup' => $this->t("You don't have media bundle of the Image type. You should <a href='!link'>create one</a>", ['!link' => $url]),
      ];
    }
    else {
      $form['media_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Media bundle'),
        '#default_value' => $this->configuration['media_type'],
        '#options' => $bundle_options,
      ];
    }

    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * @return string[]
   *   An array of bundle labels keyed by their id.
   */
  private function bundleOptions() {
    $bundle_options = [];
    $bundles = $this
      ->entityTypeManager
      ->getStorage('media_type')
      ->loadByProperties(['source' => 'image']);

    foreach ($bundles as $bundle) {
      $bundle_options[$bundle->id()] = $bundle->label();
    }

    return $bundle_options;
  }

}
